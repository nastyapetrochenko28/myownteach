<?php
session_start(); // Открытие сессии PHP
include __DIR__ . '/scripts/functions-for-all.php';
$PDO = connectDB();
?>
<!DOCTYPE html>
<html>
<head>
	<title>MyOwnTeach: Главная</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style/indexCSS.css">
	<link rel="stylesheet" type="text/css" href="style/for_allCSS.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Gabriela&display=swap" rel="stylesheet">
</head>
<body>


	<div class="mot-headpanel">
		<div class="mot-headline">
				<a href="#">MyOwnTeach</a>
		</div>
		<div class="mot-menubutton"> <a href="educational-materials.php"><b>Учебные материалы</b></a> </div>
	    <div class="mot-menubutton"> <a href="tests.php"><b>Задания для самоконтроля</b></a> </div>
	    <div class="mot-menubutton"> <a href="forum.php"><b>Форум</b></a> </div>
	    <div class="mot-menubutton"> <a href="about.php"><b>О нас </b></a></div>
	    <div class="mot-menubutton"> <b><a href="registr.php">Региcтрация</a></b></div>
	 	<? if ( ! isset($_SESSION['UID'])) {
	 		 $messege = 'Вход';
	 		 $href = "authentificacion.php";
	 		} else {
	 		 $messege = $_SESSION['UID'];
	 		 $href = "profile.php";
	 		} ?>
	 	<div class="mot-menubutton"> <b> <a href="<? echo $href ?>"><? echo $messege ?></a> </b> </div>
	</div>


	<div class="mot-contentpanel">
		<h2>Добро пожаловать на MyOwnTeach.ru</h2>
		<p align="justify">
			<tab> MyOwnTeach.ru - ресурс для самообразования нового поколения. Мы предоставляем доступ к образовательным программам и теоритическим материаллам, которые подготовленны специалистами разных направлений.
		</p>
		<h2>С нами вы можете изучать такие дисциплины как:</h2>

		<? $statement = $PDO->query('SELECT * from discipline;'); $statement->execute(); $disciplines = $statement->fetchAll();?>

		<?php foreach ($disciplines as $dis): ?>
			<div class="mot-linkbutton"> <a  href="#"><? echo $dis['Discipline'] ?></a> </div> <br>
		<?php endforeach ?>

		<p align="justify">
			<tab>Список доступных дисциплин постоянно расширяется. Пройдя регистрацию, вы можете получить доступ не только к отдельным интересующим вас темам, но и полному курсу по конкретной дисциплине.
		</p>
		<div class="mot-linkbutton mot-darklinkbutton" > <a  href="registr.php">Пройти регистрацию</a> </div>
	</div>


	<div class="mot-bottompanel">
		<div>
			<h2>Контакты</h2>
			<p>
	            <b>Контактные телефоны:</b> 8 800 500-85-75 <br>
						 8 800 780-96-95 <br>
	            <b>E-mail:</b> myownteach.contact@mail.ru <br>

	            <b>По вопросам сотрудничества:</b> <br>
	            myownteach.forpartners@mail.ru
			</p>
		    </div>
		<div>
			<h2>Навигация</h2>
			<a  href="educational-materials.php">Учебные Материаллы</a> <br>
	    	<a  href="tests.php">Задания для самоконтроля</a> <br>
			<a  href="forum.php">Формум</a> <br>
			<a  href="about.php">О нас</a> <br>
			<? if ( isset($_SESSION['UID'])) {
				// Проверка, является ли пользователь администратором
    			$statement = $PDO->prepare('
        			SELECT * from users
        			WHERE  Username = :username;');
    			$statement->execute(['username' => $_SESSION['UID']]);
    			$user = $statement->fetchAll(); foreach ($user as $us) {$type= $us['Type'];}
    			if ($type =='admin'){ $ahref='adminPanel.php'; $atext = 'Панель администратора';} 
    			else {$ahref='#'; $atext = '';}}
    		?>
			<a  href="<? echo $ahref; ?>"><? echo $atext; ?></a> <br> <!-- Ссылка на панель администратора видна иолько администраторам -->
		</div>
	</div>
</body>
</html>