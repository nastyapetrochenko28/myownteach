<?php
session_start();

$_SESSION = [];
setcookie(session_name(), '', time() - 10000);

session_destroy();

header('Location: index.php');
