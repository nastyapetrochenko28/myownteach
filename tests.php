<?php
session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title>MyOwnTeach: Задания для самоконтроля</title>
	<link rel="stylesheet" type="text/css" href="style/testsCSS.css">
	<link rel="stylesheet" type="text/css" href="style/for_allCSS.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Gabriela&display=swap" rel="stylesheet">
</head>
<body>


	<div class="mot-headpanel">
		<div class="mot-headline">
				<a href="index.php">MyOwnTeach</a>
		</div>
		<div class="mot-menubutton"> <a href="educational-materials.php"><b>Учебные материалы</b></a> </div>
	    <div class="mot-menubutton" id="mot-activebutton"> <a href="tests.php"><b>Задания для самоконтроля</b></a> </div>
	    <div class="mot-menubutton"> <a href="forum.php"><b>Форум</b></a> </div>
	    <div class="mot-menubutton"> <a href="about.php"><b>О нас </b></a></div>
	    <div class="mot-menubutton"> <b><a href="registr.php">Региcтрация</a></b></div>
	 	<? if ( ! isset($_SESSION['UID'])) {
	 		 $messege = 'Вход';
	 		 $href = "authentificacion.php";
	 		} else {
	 		 $messege = $_SESSION['UID'];
	 		 $href = "profile.php";
	 		} ?>
	 	<div class="mot-menubutton"> <b> <a href="<? echo $href ?>"><? echo $messege ?></a> </b> </div>


	<div class="mot-contentpanel" >
		
		<div id="mot-container">
			<h2>Выберите дисциплину</h2>
			<select class="mot-select" name = "blacklist">
				<option value = "2PAC">Разработка сайтовr</option>
				<option value = "50cent">Прикладное программирование</option>
				<option value = "Snoop Dogg" selected>Системное программирование</option>
				<option value = "Snoop Dogg" selected>Архитектура</option>
				<option value = "Snoop Dogg" selected>Математическая логика</option>
		    </select>
		</div>

		<div class="mot-listcontainer" id="mot-listcontainer1">
			<div class="mot-list">
				<h2>Модуль 1</h2>
<!-- 				<img src="style/856116b12399ffd41410f4e13774bf2d2.png">
 -->		    </div>
			<div class="mot-object hidden">
<!-- 				<img src="style/856116b12399ffd41410f4e13774bf2d1.png">
 -->				<a href="#">Задание 1</a>
			</div>
			<div class="mot-object hidden">
<!-- 				<img src="style/856116b12399ffd41410f4e13774bf2d1.png">
 -->				<a href="#">Задание 2</a>
			</div>
			<div class="mot-object hidden">
<!-- 				<img src="style/856116b12399ffd41410f4e13774bf2d1.png">
 -->				<a href="#">Задание 3</a>
			</div>
		</div>

		<div class="mot-listcontainer" id="mot-listcontainer2">
			<div class="mot-list">
				<h2>Модуль 2</h2>
<!-- 				<img src="style/present.png">
 -->		    </div>
			<div class="mot-object hidden">
<!-- 				<img src="style/present2.png">
 -->				<a href="#">Задание 1</a>
			</div>
			<div class="mot-object hidden">
<!-- 				<img src="style/present2.png">
 -->				<a href="#">Задание 2</a>
			</div>
			<div class="mot-object hidden">
<!-- 				<img src="style/present2.png">
 -->				<a href="#">Задание 3</a>
			</div>
		</div>

		<div class="mot-listcontainer" id="mot-listcontainer3">
			<div class="mot-list">
				<h2>Модуль 3</h2>
<!-- 				<img src="style/camara.png">
 -->		    </div>
			<div class="mot-object hidden">
<!-- 				<img src="style/camara2.jpg">
 -->				<a href="#">Задание 1</a>
			</div>
			<div class="mot-object hidden">
<!-- 				<img src="style/camara2.jpg">
 -->				<a href="#">Задание 2</a>
			</div>
			<div class="mot-object hidden">
<!-- 				<img src="style/camara2.jpg">
 -->				<a href="#">Задание 3</a>
			</div>
		</div>

		<div class="mot-listcontainer" id="mot-listcontainer4">
			<div class="mot-list">
				<h2>Модуль 4</h2>
<!-- 				<img src="style/bookicon.png">
 -->			</div>
			<div class="mot-object hidden">
<!-- 				<img src="style/bookicon2.png">
 -->				<a href="#">Задание 1</a>
			</div>
			<div class="mot-object hidden">
<!-- 				<img src="style/bookicon2.png">
 -->				<a href="#">Задание 2</a>
			</div>
			<div class="mot-object hidden">
<!-- 				<img src="style/bookicon2.png">
 -->				<a href="#">Задание 3</a>
			</div>
		</div>
	</div>


<div class="mot-bottompanel">
	<div>
		<h2>Контакты</h2>
		<p>
            <b>Контактные телефоны:</b> 8 800 500-85-75 <br>
					 8 800 780-96-95 <br>
            <b>E-mail:</b> myownteach.contact@mail.ru <br>

            <b>По вопросам сотрудничества:</b> <br>
            myownteach.forpartners@mail.ru
		</p>
	    </div>
	<div>
		<h2>Навигация</h2>
			<a  href="educational-materials.php">Учебные Материаллы</a> <br>
	    	<a  href="tests.php">Задания для самоконтроля</a> <br>
			<a  href="forum.php">Формум</a> <br>
			<a  href="about.php">О нас</a> <br>
	</div>
 </div>
<script src="scripts/openlist.js"></script>
</body>
</html>