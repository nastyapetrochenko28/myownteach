<?php session_start();?>
<!DOCTYPE html>
<html>
<head>
	<title>MyOwnTeach: О нас</title>
	<link rel="stylesheet" type="text/css" href="style/aboutCSS.css">
	<link rel="stylesheet" type="text/css" href="style/for_allCSS.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Gabriela&display=swap" rel="stylesheet">
</head>
<body>


	<div class="mot-headpanel">
		<div class="mot-headline">
				<a href="index.php">MyOwnTeach</a>
		</div>
		<div class="mot-menubutton"> <a href="educational-materials.php"><b>Учебные материалы</b></a> </div>
	    <div class="mot-menubutton"> <a href="tests.php"><b>Задания для самоконтроля</b></a> </div>
	    <div class="mot-menubutton"> <a href="forum.php"><b>Форум</b></a> </div>
	    <div class="mot-menubutton" id="mot-activebutton"> <a href="about.php"><b>О нас </b></a></div>
	    <div class="mot-menubutton"> <b><a href="registr.php">Региcтрация</a></b></div>
	 	<? if ( ! isset($_SESSION['UID'])) {
	 		 $messege = 'Вход';
	 		 $href = "authentificacion.php";
	 		} else {
	 		 $messege = $_SESSION['UID'];
	 		 $href = "profile.php";
	 		} ?>
	 	<div class=" mot-menubutton"> <b> <a href="<? echo $href ?>"><? echo $messege ?></a> </b> </div>
	</div>


	<div class="contantpanel">
		<h2>Контакты</h2>
		<p>
<b>Контактные телефоны:</b> 8 800 500-85-75<br>
		     8 800 780-96-95<br>
<b>E-mail:</b> myownteach.contact@mail.ru<br>

<b>По вопросам сотрудничества:</b><br>
myownteach.forpartners@mail.ru<br>
		</p>
		<h2>Реквизиты</h2>
		<div>
			<p>
ООО «Лабиринт.РУ»<br>
ИНН 7728644571 / КПП 771701001, 129 626, г. Москва, ул. Маломосковская, д. 22, стр. 1, этаж цокольный, пом. 1, ком 62. 
Тел./факс: +7 495 733-91-74<br>
р/с 40702810338000075568 в банке ПАО СБЕРБАНК г. Москва к/с 30101810400000000225, БИК: 044525225, ОКВЭД 47.91.2, ОКПО 84179607,
ОГРН 1077764644264.
		</p>
		</div>
		<h2></h2>
		</div>


<div class="mot-bottompanel">
	<div>
		<h2>Контакты</h2>
		<p>
            <b>Контактные телефоны:</b> 8 800 500-85-75 <br>
					 8 800 780-96-95 <br>
            <b>E-mail:</b> myownteach.contact@mail.ru <br>

            <b>По вопросам сотрудничества:</b> <br>
            myownteach.forpartners@mail.ru
		</p>
	    </div>
	<div>
		<h2>Навигация</h2>
			<a  href="educational-materials.php">Учебные Материаллы</a> <br>
	    	<a  href="tests.php">Задания для самоконтроля</a> <br>
			<a  href="forum.php">Формум</a> <br>
			<a  href="about.php">О нас</a> <br>
	</div>
 </div>
</body>
</html>