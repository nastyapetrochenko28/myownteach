<?php
session_start();
include __DIR__ . '/scripts/functions-for-all.php';
	$PDO = connectDB();
    //
?>
<!DOCTYPE html>
<html>
<head>
	<title>MyOwnTeach: Учебные материаллы</title>
	<link rel="stylesheet" type="text/css" href="style/edCSS.css">
	<link rel="stylesheet" type="text/css" href="style/for_allCSS.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Gabriela&display=swap" rel="stylesheet">
 </head>
<body>


	<div class="mot-headpanel">
		<div class="mot-headline">
			<a href="index.php">MyOwnTeach</a>
		</div>
		<div class="mot-menubutton" id="mot-activebutton"> <a href="educational-materials.html"><b>Учебные материалы</b></a> </div>
	    <div class="mot-menubutton"> <a href="tests.php"><b>Задания для самоконтроля</b></a> </div>
	    <div class="mot-menubutton"> <a href="forum.php"><b>Форум</b></a> </div>
	    <div class="mot-menubutton"> <a href="about.php"><b>О нас </b></a></div>
	    <div class="mot-menubutton"> <b><a href="registr.php">Региcтрация</a></b></div>
	 	<? if ( ! isset($_SESSION['UID'])) {
	 		 $messege = 'Вход';
	 		 $href = "authentificacion.php";
	 		} else {
	 		 $messege = $_SESSION['UID'];
	 		 $href = "profile.php";
	 		} ?>
	 	<div class="mot-menubutton"> <b> <a href="<? echo $href ?>"><? echo $messege ?></a> </b> </div>
	</div>


	<div class="mot-contentpanel" >
		
		<div id="mot-container">
			<h2>Выберите дисциплину</h2>
			<? $statement = $PDO->query('SELECT * from discipline;');
    				$statement->execute();
    				$disciplines = $statement->fetchAll(); //var_dump($disciplines);
    			?>
			<select class="mot-select" name = "blacklist">
				<?php foreach ($disciplines as $dis): ?>
					<option value = "<? echo $dis['Discipline'] ?>"> <? echo $dis['Discipline'] ?> </option>
				<? endforeach ?>
		    </select>
		</div>

		<div class="mot-listcontainer" id="mot-listcontainer1">
			<div class="mot-list" id="mot-list1">
				<? $statement1 = $PDO->prepare('
        			SELECT * from materials
        			WHERE Type = "Лекции";');
    				$statement1->execute();
    				$files = $statement1->fetchAll();
    			?>
				<h2>Лекции</h2>
				<img src="style/Pictures/856116b12399ffd41410f4e13774bf2d2.png">
		    </div>
		    <?php foreach ($files as $file): ?>
				<div class="mot-object hidden">
					<img src="style/Pictures/856116b12399ffd41410f4e13774bf2d1.png">
					<a href="<? echo $filepath ?>"><? echo $file['Filename'] ?></a>
				</div>
			<?php endforeach ?>
		</div>

		<div class="mot-listcontainer" id="mot-listcontainer2">
			<div class="mot-list" id="mot-list2">
				<? $statement1 = $PDO->prepare('
        			SELECT * from materials
        			WHERE Type = "Презентации";');
    				$statement1->execute();
    				$files = $statement1->fetchAll();
    			?>
				<h2>Презентации</h2>
				<img src="style/Pictures/present.png">
		    </div>
		    <?php foreach ($files as $file): ?>
				<div class="mot-object hidden">
					<img src="style/Pictures/present2.png">
					<a href="<? echo $filepath ?>"><? echo $file['Filename'] ?></a>
				</div>
			<?php endforeach ?>
		</div>

		<div class="mot-listcontainer" id="mot-listcontainer3">
			<div class="mot-list" id="mot-list3">
				<? $statement1 = $PDO->prepare('
        			SELECT * from materials
        			WHERE Type = "Видеоуроки";');
    				$statement1->execute();
    				$files = $statement1->fetchAll();
    			?>
				<h2>Видеоуроки</h2>
				<img src="style/Pictures/camara.png">
		    </div>
		    <?php foreach ($files as $file): ?>
				<div class="mot-object hidden">
					<img src="style/Pictures/camara2.jpg">
					<a href="<? echo $filepath ?>"><? echo $file['Filename'] ?></a>
				</div>
			<?php endforeach ?>
		</div>

		<div class="mot-listcontainer" id="mot-listcontainer4">
			<div class="mot-list" id="mot-list4">
				<? $statement1 = $PDO->prepare('
        			SELECT * from materials
        			WHERE Type = "Учебники";');
    				$statement1->execute();
    				$files = $statement1->fetchAll();
    			?>
				<h2>Учебники</h2>
				<img src="style/Pictures/bookicon.png">
			</div>
			<?php foreach ($files as $file): ?>
				<div class="mot-object hidden">
					<img src="style/Pictures/bookicon2.png">
					<a href="<? echo $filepath ?>"><? echo $file['Filename'] ?></a>
				</div>
			<?php endforeach ?>
		</div>
	</div>


	<div class="mot-bottompanel">
		<div>
			<h2>Контакты</h2>
			<p>
	            <b>Контактные телефоны:</b> 8 800 500-85-75 <br>
						 8 800 780-96-95 <br>
	            <b>E-mail:</b> myownteach.contact@mail.ru <br>

	            <b>По вопросам сотрудничества:</b> <br>
	            myownteach.forpartners@mail.ru
			</p>
		    </div>
		<div>
			<h2>Навигация</h2>
			<a  href="educational-materials.php">Учебные Материаллы</a> <br>
	    	<a  href="tests.php">Задания для самоконтроля</a> <br>
			<a  href="forum.php">Формум</a> <br>
			<a  href="about.php">О нас</a> <br>
		</div>
	</div>

	<script src="scripts/openlist.js"></script>
</body>
</html>