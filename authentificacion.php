<?php

include __DIR__ . '/scripts/functions-for-all.php';

function authentificacion(): ?string
{
    $mistakes = 0;
    if ($_SERVER['REQUEST_METHOD'] !== 'POST')
    {
        return false;
    }
    else
    {
        $username = $_POST['user_Name'] ?? null;
        $password = $_POST['user_Pass'] ?? null;

        if ( trim($username) == "") {
            ++$mistakes;
            echo 'Логин обязателен для заполнения';
            return null;
        }
       
        if ( ! trim($password)) {
            ++$mistakes;
            echo 'Пароль обязателен для заполнения';
            return null;
        }

        $PDO = connectDB();

        $user_ch = getUserByUsername($PDO, $username);

        if ($user_ch==null)
        {
        	echo 'Неправльный логин'; 
            ++$mistakes;
            return null;
        }
        else 
        {
        	if ( $password != $user_ch['Password']) {
	        	echo 'Неправльный пароль';
	        	++$mistakes;
                return null;
	        }
        }

        if ($mistakes==0) {
            startSession();
		    $_SESSION['UID'] = $_POST['user_Name'];
            $_SESSION['time'] = date("H:i:s");
		    header('Location: ' . 'index.php?' . session_name() . '=' . session_id());
        }
    }
    return null;
}
$message = authentificacion();

?>

<!DOCTYPE html>
<html>
<head>
		<title>MyOwnTeach: Вход</title>
		<link rel="stylesheet" type="text/css" href="style/registrCSS.css">
		<link rel="stylesheet" type="text/css" href="style/for_allCSS.css">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Gabriela&display=swap" rel="stylesheet">
</head>
<body>
     <div class="mot-headline">
		<a href="/myownteach_ve3/index.php">MyOwnTeach</a>
	</div>

	<div class="mot-inputForm">
		<form id="mot-subscribtionForm" method="POST" action="authentificacion.php">
			<div>
				<label for="mot-name">Имя пользователя</label>
				<input type="text" id="mot-name" name="user_Name">
			</div>	
			<div>
				<label for="mot-pass">Пароль</label>
				<input type="password" id="mot-pass" name="user_Pass">
			</div>		
			<div>
				<button type="submit" name="authentificacion" class="mot-button">Войти</button>
			</div>
		</form>
	</div>

	<script src="scripts/PlaceHolder.js"></script>
</body>
</html>