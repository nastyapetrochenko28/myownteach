<?php 
    session_start();
    include __DIR__ . '/scripts/protection.php';
    include __DIR__ . '/scripts/functions-for-all.php';
    $PDO = connectDB();
         if (!$PDO) {
             die ('Ошибка соединения!' . mysqli_connect_error());
         }

    // Проверка, является ли пользователь администратором
    $statement = $PDO->prepare('
        SELECT * from users
        WHERE  Username = :username;');
    $statement->execute(['username' => $_SESSION['UID']]);
    $user = $statement->fetchAll(); foreach ($user as $us) {$type= $us['Type'];}
    if ($type!='admin'){echo 'Доступ запрещен'; die();}

    //Вывод таблицы с файлами
    $statement = $PDO->prepare('
        SELECT * from materials');
    $statement->execute();
    $files = $statement->fetchAll();
    //

    function uploadIntoDB (PDO $PDO): ?string
    {
        $mistakes = 0;
        if ($_SERVER['REQUEST_METHOD'] !== 'POST')
        {
            return false;
        }
        else
        {
            $discipline = $_POST['discipline'] ?? null;
            $type = $_POST['type'] ?? null;
            $filename = ($_FILES['file']['name']) ?? null;

            // print_r($discipline, true);
            // print_r($type, true);
            // print_r($filename, true);

            $file_ch = getFilerByFilename($PDO, $discipline, $type, $filename);
            if ($user_ch)
            {
                ++$mistakes;
                return 'Такой файл уже существует. если требуется сохранить данный файл, как новый, переименуйте его и повторите попытку. Если нужно заменить уже существующий файл, удалите из базы старую версию и повторите попытку';
            }

            if($mistakes==0){
                $success = createFileField ($PDO, $discipline, $type, $filename);
            }
        }
        return null;
    }
    //

    //Загрузка файла
    $isSucceed = false;
    $uploadDir = __DIR__ . "/uploads/";
    $IsPostMEthod = $_SERVER['REQUEST_METHOD'] === 'POST';

    if ($IsPostMEthod) {
        $object = $_FILES['file'];
        $destination = $uploadDir . $object['name'];

        $isSucceed = move_uploaded_file($object['tmp_name'], $destination);        
    }

    $message = uploadIntoDB($PDO);

    if ($IsPostMEthod) {    
        if ($isSucceed) {
            //header('Location: /myownteach_ve3/adminPanel.php');
            redirect('adminPanel.php');
        } else {
            echo 'Не удалось загрузить файл';
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title>MyOwnTeach: Панель администратора</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style/aPanelCSS.css">
	<link rel="stylesheet" type="text/css" href="style/for_allCSS.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Gabriela&display=swap" rel="stylesheet">
</head>
<body>

<div class="mot-headpanel">
		<div class="mot-headline">
				<a href="index.php">MyOwnTeach</a>
		</div>
		<div class="mot-menubutton"> <a href="educational-materials.php"><b>Учебные материалы</b></a> </div>
	    <div class="mot-menubutton"> <a href="tests.php"><b>Задания для самоконтроля</b></a> </div>
	    <div class="mot-menubutton"> <a href="forum.php"><b>Форум</b></a> </div>
	    <div class="mot-menubutton"> <a href="about.php"><b>О нас </b></a></div>
	    <div class="mot-menubutton"> <b><a href="registr.php">Региcтрация</a></b></div>
	 	<? if ( ! isset($_SESSION['UID'])) {
             $messege = 'Вход';
             $href = "authentificacion.php";
            } else {
             $messege = $_SESSION['UID'];
             $href = "profile.php";
            } ?>
        <div class="mot-menubutton"> <b> <a href="<? echo $href ?>"><? echo $messege ?></a> </b> </div>
</div>


<div class="mot-contentpanel">

    <div class="mot-TableFiles">
            <h2>Список файлы</h2>
            <table class="table">
                <thead>
                    <tr>
                        <th>Дисциплина</th>
                        <th>Формат материалла</th>
                        <th>Имя файла</th>
                        <th>Размер</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($files as $file): ?>
                        <tr>
                            <td><?php echo $file['Discipline'] ?></td>
                            <td><?php echo $file['Type'] ?></td>
                            <td><?php echo $file['Filename'] ?></td>
                            <td></td>
                            <?php $filepath = trim("/myownteach_ve3/uploads/"). $file['Filename'];?>
                            <td><a href="scripts/delete-file-admin.php?filename=<?=$file['Filename']?>">Удалить</a></td>
                            <td><a target="_blank" href="<?php echo $filepath ?>">Читать онлайн</a></td>
                              
                           
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
    </div>

    <form method="post" class="mot-addFileForm" enctype="multipart/form-data" action="">
        <h2>Добавить контент</h2>
        <div>
            <div class="mot-labeldiv"><label for="discipline">Дисциплина</label></div>
            <select class="mot-select" name = "discipline">
                <option value = "Разработка_сайтов">Разработка сайтов</option>
                <option value = "Прикладное_программирование">Прикладное программирование</option>
                <option value = "Системное_программирование" selected>Системное программирование</option>
                <option value = "Архитектура" selected>Архитектура</option>
                <option value = "Математическая_логика" selected>Математическая логика</option>
                </select>
        </div>  
        <div>
            <div class="mot-labeldiv"><label for="mot-filetype">Формат материалла</label></div>
            <select class="mot-select" name = "type">
                <option value = "Лекции">Лекции</option>
                <option value = "Презентации">Презентации</option>
                <option value = "Видеоуроки" selected>Видеоуроки</option>
                <option value = "Учебники" selected>Учебники</option>
                </select>
        </div>
        <div>
            <div class="mot-labeldiv"><label for="filename">Файл</label></div>
           <input class="mot-control" type="file" name="file" />
        </div>
        <button class="mt-2 btn btn-success">Загрузить</button>
    </form>

</div>


<div class="mot-bottompanel">
    <div>
        <h2>Контакты</h2>
        <p>
            <b>Контактные телефоны:</b> 8 800 500-85-75 <br>
                     8 800 780-96-95 <br>
            <b>E-mail:</b> myownteach.contact@mail.ru <br>

            <b>По вопросам сотрудничества:</b> <br>
            myownteach.forpartners@mail.ru
        </p>
        </div>
    <div>
        <h2>Навигация</h2>
        <a  href="educational-materials.php">Учебные Материаллы</a> <br>
        <a  href="tests.php">Задания для самоконтроля</a> <br>
        <a  href="forum.php">Формум</a> <br>
        <a  href="about.php">О нас</a> <br>
        <a  href="#">По вопросам сотрудничества</a> <br>
    </div>
</div>
</body>
</html>