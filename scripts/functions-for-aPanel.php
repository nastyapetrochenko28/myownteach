<?php
function connectDB():PDO{
	return new PDO('mysql:host=localhost; dbname=movedb;', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
}
function getFilerByFilename(PDO $PDO, string $discipline, string $type, string $filename): ?array {
    $stmt = $PDO->prepare('
    	SELECT * FROM materials WHERE Discipline = :discipline AND Type = :type AND Filename = :filename');
    $stmt->execute([
    	'discipline' => $discipline, 
    	'type'=> $type, 
    	'filename'=> $filename
    ]);
    return $stmt->fetch() ?: null;
}

function createFileField(PDO $PDO, string $discipline, string $type, string $filename): bool {
    $stmt = $PDO->prepare('
        INSERT INTO materials (Discipline, Type, Filename) 
        VALUE (:discipline, :type, :filename)
    '); 

    return $stmt->execute([
        'discipline' => $discipline, 
    	'type'=> $type, 
    	'filename'=> $filename
    ]);
}

function deleteFromDB(PDO $PDO, string $filename): bool {
	$stmt = $PDO->prepare('
        DELETE FROM materials 
        WHERE Filename = :filename
    '); 

    return $stmt->execute([
        'filename' => $filename
    ]);
}
?>
