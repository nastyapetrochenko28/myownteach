<?php

// Создание нового пользователя
function createUser(PDO $pdo, $username, $email, $password): bool {
    $stmt = $pdo->prepare('
        INSERT INTO users (Username, Email, Password) 
        VALUE (:usermame, :email, :password)
    '); // Подготавливаем sql запрос на добавление пользователя, указываем параменты :login и :password

    return $stmt->execute([
        'usermame' => $username,
        'email'=> $email,
        'password' => password_hash($password, PASSWORD_BCRYPT),
    ]);
}

// Функция получает пользователя по логину
function getUserByUsername(PDO $pdo, string $username): ?array {
    $stmt = $pdo->prepare('SELECT * FROM users WHERE username = :username');
    $stmt->execute(['username' => $username]);
    return $stmt->fetch() ?: null;
}

// Функция получает пользователя по email
function getUserByEmail(PDO $pdo, string $email): ?array {
    // var_dump($email);die();
    $stmt = $pdo->prepare('SELECT * FROM users WHERE email = :email'); 
    $stmt->execute(['email' => $email]);
    return $stmt->fetch() ?: null; 
}