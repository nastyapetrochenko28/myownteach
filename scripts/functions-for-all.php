<?
function redirect(string $resource): void {
    header('Location: ' . $resource); // Отправляем заголовок location
    die();
}

function startSession() {
    if (session_status() === PHP_SESSION_DISABLED) { // Проверяем включены ли сессии на сервере
        echo 'Сессии выключены'; // Если отключены, выводим сообщение
        die(); // И завершаем выполнение скрипта
    }

    if (session_status() === PHP_SESSION_NONE) { // Если сессия отключена
        session_start(); // Запускаем её
    }
}

///////////////////////////////////Функции БД/////////////////////////////////////////////////

function connectDB():PDO{
	return new PDO('mysql:host=localhost; dbname=movedb;', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
}

function getFilerByFilename(PDO $PDO, string $discipline, string $type, string $filename): ?array {
    $stmt = $PDO->prepare('
    	SELECT * FROM materials WHERE Discipline = :discipline AND Type = :type AND Filename = :filename');
    $stmt->execute([
    	'discipline' => $discipline, 
    	'type'=> $type, 
    	'filename'=> $filename
    ]);
    return $stmt->fetch() ?: null;
}

function createFileField(PDO $PDO, string $discipline, string $type, string $filename): bool {
    $stmt = $PDO->prepare('
        INSERT INTO materials (Discipline, Type, Filename) 
        VALUE (:discipline, :type, :filename)
    '); 

    return $stmt->execute([
        'discipline' => $discipline, 
    	'type'=> $type, 
    	'filename'=> $filename
    ]);
}

function deleteFromDB(PDO $PDO, string $filename): bool {
	$stmt = $PDO->prepare('
        DELETE FROM materials 
        WHERE Filename = :filename
    '); 

    return $stmt->execute([
        'filename' => $filename
    ]);
}


function createUser(PDO $pdo, $username, $email, $password): bool {
    $stmt = $pdo->prepare('
        INSERT INTO users (Username, Email, Password) 
        VALUE (:usermame, :email, :password)
    '); // Подготавливаем sql запрос на добавление пользователя, указываем параменты :login и :password

    return $stmt->execute([
        'usermame' => $username,
        'email'=> $email,
        'password' => password_hash($password, PASSWORD_BCRYPT),
    ]);
}

function getUserByUsername(PDO $pdo, string $username): ?array {
    $stmt = $pdo->prepare('SELECT * FROM users WHERE username = :username');
    $stmt->execute(['username' => $username]);
    return $stmt->fetch() ?: null;
}

function getUserByEmail(PDO $pdo, string $email): ?array {
    // var_dump($email);die();
    $stmt = $pdo->prepare('SELECT * FROM users WHERE email = :email'); 
    $stmt->execute(['email' => $email]);
    return $stmt->fetch() ?: null; 
}
?>