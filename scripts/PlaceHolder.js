let form = document.getElementById('mot-subscribtionForm');
let labels = form.getElementsByTagName('label');

for (let element of form.getElementsByTagName('input')) {
	AddLabelsProcessing(element.getAttribute('id'));
}

function AddLabelsProcessing(inputId) {
	let input = document.getElementById(inputId);
	let label;

	for (let element of labels) {
		if (element.getAttribute('for')==input.getAttribute('id')) {
			label = element;
			break;
		}
	}

	input.addEventListener('focusin',(event)=>{
		label.classList.add('mot-label-up');
	});


	input.addEventListener('focusout',(event)=>{
		if (input.value=='') {
			label.classList.remove('mot-label-up');	
		}
	});
}	
