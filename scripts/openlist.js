let panel = document.querySelector('.mot-contentpanel');
let objects = document.getElementsByClassName('mot-object');

panel.addEventListener('click',(event)=>{
	if(event.target.className == 'mot-list'){
    	let parent = event.target.parentNode;

    	for(element of objects) {
	    	if (element.classList.contains('hidden') && element.parentNode.id == parent.id){
		    	element.classList.remove('hidden');
			} else if(!element.classList.contains('hidden') && element.parentNode.id == parent.id){
		    	element.classList.add('hidden');
			}
		}
    }
});


// let form = document.getElementById('vsp-subscribtionform');
// let labels = form.getElementsByTagName('label');

// for (let element of form.getElementsByTagName('input')) {
// 	AddLabelsProcessing(element.getAttribute('id'));
// }

// function AddLabelsProcessing(inputId) {
// 	let input = document.getElementById(inputId);
// 	let label;

// 	for (let element of labels) {
// 		if (element.getAttribute('for')==input.getAttribute('id')) {
// 			label = element;
// 			break;
// 		}
// 	}

// 	input.addEventListener('focusin',(event)=>{
// 		label.classList.add('vsp-label-up');
// 	});


// 	input.addEventListener('focusout',(event)=>{
// 		if (input.value=='') {
// 			label.classList.remove('vsp-label-up');	
// 		}
// 	});
// }	
