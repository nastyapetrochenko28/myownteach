<?php
 include __DIR__ . '/scripts/functions-for-all.php';
     $PDO = new PDO('mysql:host=localhost; dbname=movedb;', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING)); // "localhost", "root", "root", "movedb"
     if (!$PDO) {
         die ('Ошибка соединения!' . mysqli_connect_error());
     }
     // echo 'Успешное соедининение';

function registr (PDO $PDO): ?string
{
    $mistakes = 0;
    if ($_SERVER['REQUEST_METHOD'] !== 'POST')
    {
        return false;
    }
    else
    {
        $username = $_POST['user_Name'] ?? null;
        $email = $_POST['user_E-mail'] ?? null;
        $password = $_POST['user_Pass'] ?? null;
        $rPassword = $_POST['user_RPass'] ?? null;

        // var_dump($username);
        // var_dump($email);
        // var_dump($password);
        // var_dump($rPassword);

        if ( ! trim($username)) {
            ++$mistakes;
            return 'Логин обязателен для заполнения';
        }

        if ( ! trim($password)) {
            ++$mistakes;
            return 'Пароль обязателен для заполнения';
        }

        if ( ! trim($rPassword)) {
            ++$mistakes;
            return 'Повторите пароль';
        }

        if ($password !== $rPassword) {
            ++$mistakes;
            return 'Пароли не совпадают';
        }


        $user_ch = getUserByUsername($PDO, $username);
        if ($user_ch)
        {
            ++$mistakes;
            return 'Пользователь с таким именем уже существует';
        }

        $email_ch = getUserByEmail($PDO, $email);
        if ($email_ch)
        {
            ++$mistakes;
            return 'Пользователь с таким E-mail уже существует';
        }

        if($mistakes==0){
            $success = createUser($PDO, $username, $email, $password);
        redirect('authentificacion.php');
        }
    }
    return null;
}
$message = registr($PDO);
?>

<!DOCTYPE html>
<html lang charset="UTF-8">
<head>
	<title>MyOwnTeach</title>
    <meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="style/registrCSS.css">
	<link rel="stylesheet" type="text/css" href="style/for_allCSS.css">
	<!-- <link rel="preconnect" href="https://fonts.gstatic.com"> -->
	<!-- <link href="https://fonts.googleapis.com/css2?family=Gabriela&display=swap" rel="stylesheet"> -->
</head>
<body>

	<div class="mot-headline">
		<a href="index.php">MyOwnTeach</a>
	</div>
	<div class="mot-inputForm">
        <?php echo $message ?>
		<form id="mot-subscribtionForm" method="post" action="registr.php">
			<div>
				<label for="mot-name">Имя пользователя</label>
				<input type="text" id="mot-name" name="user_Name">
			</div>	
			<div>
				<label for="mot-E-mail">E-mail</label>
				<input type="email" id="mot-E-mail" name="user_E-mail">
			</div>
			<div>
				<label for="mot-pass">Пароль</label>
				<input type="password" id="mot-pass" name="user_Pass">
			</div>
            <div>
                <label for="mot-rPass">Повторите пароль</label>
                <input type="password" id="mot-rPass" name="user_RPass">
            </div>
			<div>
                <button type="submit" name="registr" class="mot-button">Зарегистрироваться</button>
			</div>
		</form>
	</div>

	<script src="scripts/PlaceHolder.js"></script>
</body>
</html>