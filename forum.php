﻿<!DOCTYPE html>
<html>
<head>
	<title>MyOwnTeach: Форум</title>
	<link rel="stylesheet" type="text/css" href="style/forumCSS.css">
	<link rel="stylesheet" type="text/css" href="style/for_allCSS.css">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Gabriela&display=swap" rel="stylesheet">
</head>
<body>


	<div class="mot-headpanel">
		<div class="mot-headline">
				<a href="index.php">MyOwnTeach</a>
		</div>
		<div class="mot-menubutton"> <a href="educational-materials.php"><b>Учебные материалы</b></a> </div>
	    <div class="mot-menubutton"> <a href="tests.php"><b>Задания для самоконтроля</b></a> </div>
	    <div class="mot-menubutton" id="mot-activebutton"> <a href="forum.php"><b>Форум</b></a> </div>
	    <div class="mot-menubutton"> <a href="about.php"><b>О нас </b></a></div>
	    <div class="mot-menubutton"> <b><a href="registr.php">Региcтрация</a></b></div>
	 	<? if ( ! isset($_SESSION['UID'])) {
	 		 $messege = 'Вход';
	 		 $href = "authentificacion.php";
	 		} else {
	 		 $messege = $_SESSION['UID'];
	 		 $href = "profile.php";
	 		} ?>
	 	<div class="mot-menubutton"> <b> <a href="<? echo $href ?>"><? echo $messege ?></a> </b> </div>


</body>
</html>